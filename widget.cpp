#include "widget.h"
#include "ui_widget.h"
#include <QMessageBox>


Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    checkMoney();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::checkMoney() {
    if(money < 100) {
        ui->pbCoffee->setEnabled(false);
        ui->pbTea->setEnabled(false);
	ui->pbMilk->setEnabled(false);
    }
    else if(money < 150) {
        ui->pbCoffee->setEnabled(true);
        ui->pbTea->setEnabled(false);
        ui->pbMilk->setEnabled(false);
    }
    else if(money < 200) {
        ui->pbCoffee->setEnabled(true);
        ui->pbTea->setEnabled(true);
        ui->pbMilk->setEnabled(false);
    }
    else if (money < 0) {
        money = 0;
    }
    else if (money > 99990) {
        money = 99990;
    }
    else {
        ui->pbCoffee->setEnabled(true);
        ui->pbTea->setEnabled(true);
        ui->pbMilk->setEnabled(true);
    }
}

void Widget::changeMoney(int diff) {
    money += diff;
    ui->lcdNumber->display(money);
    checkMoney();
}


void Widget::on_pb10_clicked()
{
    changeMoney(10);
}


void Widget::on_pb50_clicked()
{
    changeMoney(50);
}


void Widget::on_pb100_clicked()
{
    changeMoney(100);
}


void Widget::on_pb500_clicked()
{
    changeMoney(500);
}


void Widget::on_pbCoffee_clicked()
{
    changeMoney(-100);
}


void Widget::on_pbTea_clicked()
{
    changeMoney(-150);
}

void Widget::on_pbMilk_clicked()
{
    changeMoney(-200);
}

void Widget::on_pbReset_clicked()
{
    int coins[] = {500, 100, 50, 10};
    QString message = "";

    for (const int& coin : coins) {
        message += QString("%1원: %2개\n").arg(coin).arg(money / coin);
        money %= coin;
    }

    QMessageBox mb;
    mb.information(this, "거스름돈", message);
    changeMoney(0);
}
